package bank;

import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class Bank {
	
	HashMap<Integer,Person> persons;
	PatternObserver pObs;
	Set<HashMap.Entry<Integer,Person>> myPersons;
	public GUI window;
	
	public Bank(GUI bankGUI) {
		window = bankGUI;
		persons = new HashMap<Integer,Person>();
		myPersons =  persons.entrySet();
		pObs = new PatternObserver();
		/*ObjectInputStream o = null;
		File inputFile = new File("Persons.txt");
		FileInputStream persInput = null;
		try {
			persInput = new FileInputStream(inputFile);
		} catch (FileNotFoundException e1) {
			System.out.println("File not found!");
			e1.printStackTrace();
		}
		
		
		try {
			o = new ObjectInputStream(persInput);
		} catch (IOException e) {
			System.out.println("Input stream not found!");
			e.printStackTrace();
		}
		
		ObjectInputStream o2 = null;
		File inputFile2 = new File("Accounts.txt");
		FileInputStream accInput = null;
		try {
			accInput = new FileInputStream(inputFile2);
		} catch (FileNotFoundException e1) {
			System.out.println("File not found!");
			e1.printStackTrace();
		}
		
		try {
			o2 = new ObjectInputStream(accInput);
		} catch (IOException e) {
			System.out.println("Input stream not found!");
			e.printStackTrace();
		}

		
		Person p = null;
		do {
			try {
				p = (Person) o.readObject();
				for (Account a: p.accs) {
					a= (Account) o2.readObject();
					p.accs.add(a);
				}
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			persons.put(p.getID(), p);
		} while (p!=null);
		*/
		
		
	}
	
	public void addPerson(String n, Integer i) {
		Person p = new Person(n,i);
		boolean exists = false;
		for (HashMap.Entry<Integer,Person> e : myPersons) {
			if (e.getKey() == i) exists = true;
		}
		if (exists) {
			this.window.textFieldConsole.setText("A person with this ID already exists!");
		}
		else 
			persons.put(i, p);
			this.window.textFieldConsole.setText("A person with ID: " + i + " and name: " + n + " added.");
			window.refreshPersons();
	}
	
	public void removePerson(Integer id) {
		boolean exists = false;
		for (HashMap.Entry<Integer,Person> e : myPersons) {
			if (e.getKey() == id) exists = true;
		}
		if (exists) {
			persons.remove(id);
			this.window.textFieldConsole.setText("Person removed.");
		}
		else {
			this.window.textFieldConsole.setText("Person does not exist.");
		}
		window.refreshPersons();
	}
	
	public void addSavingAccount(Integer pid, Integer aid) {
		Person p = persons.get(pid);
		if (p == null) {
			this.window.textFieldConsole.setText("Person not found!");
			return;
		}
		else {
			boolean exists = false;
			for (Account a: p.accs) {
				if (a.getID() == aid) exists = true;
			}
			if(exists) {
				this.window.textFieldConsole.setText("Account already exsists!");
				return;
			}
			else {
				this.window.textFieldConsole.setText("Saving account added!");
				p.accs.add(new SavingAccount(p,aid));
				pObs.accModified(p);
				window.refreshPersons();
			}
		}
	}
	
	public void removeAccount(Integer pid, Integer aid) {
		Account r=null;
		Person p = persons.get(pid);
		Iterator<Account> it = p.accs.iterator();
		while (it.hasNext()) {
			r = it.next();
			if (r.getID() == aid) {
				it.remove();
				this.window.textFieldConsole.setText("Account removed.");
				pObs.accModified(p);
				window.refreshPersons();
				break;
			}
		}
		if (r==null) {
			this.window.textFieldConsole.setText("Account not found!");
		}
	}
	
	public void addSpendingAccount(Integer pid, Integer aid) {
		Person p = persons.get(pid);
		if (p == null) {
			this.window.textFieldConsole.setText("Person not found!");
			return;
		}
		else {
			boolean exists = false;
			for (Account a: p.accs) {
				if (a.getID() == aid) exists = true;
			}
			if(exists) {
				this.window.textFieldConsole.setText("Account already exsists!");
				return;
			}
			else {
				this.window.textFieldConsole.setText("Spending account added!");
				pObs.accModified(p);
				p.accs.add(new SpendingAccount(p,aid));
				window.refreshPersons();
			}
		}
	}
	
	public Account getAccount(Integer pid, Integer aid) {
		Account a = null;
		Person p = persons.get(pid);
		if (p == null) {
			this.window.textFieldConsole.setText("Person not found!");
			return null;
		}
		for (Account acc: p.accs) {
			if (acc.getID() == aid) a = acc;
		}
		if (a == null) {
			this.window.textFieldConsole.setText("Account not found!");
		}
		else {
			this.window.textFieldConsole.setText("The account's current balance is: "+ a.getBalance());
		}
		return a;
	}
	
	public void deposit(Integer pid, Integer aid, Integer sum) {
		Account a = getAccount(pid,aid);
		this.window.textFieldConsole.setText(a.deposit(sum));
		pObs.accModified(persons.get(pid));
		window.refreshPersons();
	}
	
	public void withdraw(Integer pid, Integer aid, Integer sum) {
		Account a = getAccount(pid,aid);
		this.window.textFieldConsole.setText(a.withdraw(sum));
		pObs.accModified(persons.get(pid));
		window.refreshPersons();
	}
	
}
