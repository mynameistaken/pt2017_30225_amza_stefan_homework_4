package bank;

import java.util.ArrayList;

public class Person implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5567299379742000045L;
	private String name;
	private Integer id;
	private ArrayList<String> notes;
	public ArrayList<Account> accs = new ArrayList<Account>();
	
	public Person(String n, Integer i) {
		name = n;
		id = i;
		notes = new ArrayList<String>();
	}
	
	public String getName() {
		return name;
	}
	
	public Integer getID() {
		return id;
	}
	
	void addNote(String note) {
		notes.add(note);
	}
	
	void displayNotes() {
		if (notes.isEmpty()) System.out.println("This person has no notifications!");
		else {
			System.out.println("Notes of " + name + "(ID=" + id + "):");
			for (String s : notes) {
				System.out.println(s);
			}
		}
	}
}
