package bank;

public class SpendingAccount extends Account {

	public SpendingAccount(Person h, Integer i) {
		super(h, i);
	}
	
	public String toString() {
		return "Spending Account";
	}
}
