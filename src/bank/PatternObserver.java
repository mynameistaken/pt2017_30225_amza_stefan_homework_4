package bank;

public class PatternObserver {
	public void accModified(Person p) {
		System.out.println("To " + p.getName() + ": A modification has been made to one of your accounts.");
	}
}
