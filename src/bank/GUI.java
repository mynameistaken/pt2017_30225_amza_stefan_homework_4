package bank;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;
import javax.swing.JScrollBar;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class GUI {

	public JFrame frame;
	public JTextField textFieldConsole;
	public JTable table;
	public JTable table_1;
	private Bank b;
	private DefaultTableModel accModel;
	private DefaultTableModel persModel;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		GUI window = new GUI();
		window.frame.setVisible(true);
		Bank b = new Bank(window);
		window.b = b;
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}
	
	//close operation
	
	

	/**
	 * Initialize the contents of the frame.
	 */
	
	
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(210, 105, 30));
		frame.setBounds(100, 100, 1253, 655);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		/*
		//serialization output
		try {
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		    	File outputFile = new File("Persons.txt");
				FileOutputStream persOutput = null;
				ObjectOutputStream o = null;
				ObjectOutputStream o2 =null;
				try {
					persOutput = new FileOutputStream(outputFile);
				} catch (FileNotFoundException e1) {
					System.out.println("File not found!");
					e1.printStackTrace();
				}
				
				try {
					 o = new ObjectOutputStream(persOutput);
				} catch (IOException e) {
					System.out.println("Output stream not found!");
					e.printStackTrace();
				}
				try {
				for (HashMap.Entry<Integer,Person> e :b.myPersons) {
					try {
						o.writeObject(e.getValue());
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						System.exit(0);
						e1.printStackTrace();
					} 
				}
				}
				catch (NullPointerException e1) {
					// TODO Auto-generated catch block
					System.exit(0);
					e1.printStackTrace();
				}
				
				
				File outputFile2 = new File("Accounts.txt");
				FileOutputStream accOutput = null;
				try {
					accOutput = new FileOutputStream(outputFile2);
				} catch (FileNotFoundException e1) {
					System.out.println("File not found!");
					e1.printStackTrace();
				}
				
				try {
					o2 = new ObjectOutputStream(accOutput);
				} catch (IOException e) {
					System.out.println("Output stream not found!");
					e.printStackTrace();
				}
				
				for (HashMap.Entry<Integer,Person> e :b.myPersons) {
					try {
						for (Account a: e.getValue().accs) {
							o2.writeObject(a);
						}
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
		    	System.exit(0);
		    }
		});
		} catch(NullPointerException e) {
			System.exit(0);
		}
		*/
		
		
		persModel = new DefaultTableModel();
		persModel.addColumn("Name");
		persModel.addColumn("ID");
		accModel = new DefaultTableModel();
		accModel.addColumn("ID");
		accModel.addColumn("Balance");
		accModel.addColumn("Type");
		accModel.addColumn("Holder ID");
		
		JLabel lblPersons = new JLabel("Persons");
		lblPersons.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblPersons.setBounds(12, 13, 100, 33);
		frame.getContentPane().add(lblPersons);
		
		JLabel lblAccounts = new JLabel("Messages");
		lblAccounts.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblAccounts.setBounds(727, 13, 100, 33);
		frame.getContentPane().add(lblAccounts);
		
		JTextArea textAddPID = new JTextArea();
		textAddPID.setToolTipText("Enter ID");
		textAddPID.setBounds(272, 21, 421, 25);
		frame.getContentPane().add(textAddPID);
		
		JTextArea textRemovePID = new JTextArea();
		textRemovePID.setToolTipText("Enter ID");
		textRemovePID.setBounds(272, 107, 421, 25);
		frame.getContentPane().add(textRemovePID);
		
		JTextArea txtrAddPName = new JTextArea();
		txtrAddPName.setToolTipText("Enter name");
		txtrAddPName.setBounds(272, 59, 421, 25);
		frame.getContentPane().add(txtrAddPName);
		
		JButton btnAddPerson = new JButton("Add person");
		btnAddPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
				b.addPerson(txtrAddPName.getText(),Integer.parseInt(textAddPID.getText()));
				}
				catch(NumberFormatException e){
					textFieldConsole.setText("Invalid format!");
					e.printStackTrace();
				}
			}
		});
		btnAddPerson.setBounds(103, 20, 157, 25);
		frame.getContentPane().add(btnAddPerson);
		
		JButton btnRemovePerson = new JButton("Remove person");
		btnRemovePerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				b.removePerson(Integer.parseInt(textRemovePID.getText()));
			}
		});
		btnRemovePerson.setBounds(103, 106, 157, 25);
		frame.getContentPane().add(btnRemovePerson);
		
		JTextArea textAddAID = new JTextArea();
		textAddAID.setToolTipText("Enter account ID");
		textAddAID.setBounds(272, 181, 421, 25);
		frame.getContentPane().add(textAddAID);
		
		JTextArea textAddAPID = new JTextArea();
		textAddAPID.setToolTipText("Enter holder ID");
		textAddAPID.setBounds(272, 219, 421, 25);
		frame.getContentPane().add(textAddAPID);
		
		JButton btnNewButton = new JButton("Add saving account");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
				b.addSavingAccount(Integer.parseInt(textAddAPID.getText()),Integer.parseInt(textAddAID.getText()));
				}
				catch(NumberFormatException e){
					textFieldConsole.setText("Invalid format!");
					e.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(103, 180, 157, 25);
		frame.getContentPane().add(btnNewButton);
		
		JTextArea textRemoveAID = new JTextArea();
		textRemoveAID.setToolTipText("Enter account ID");
		textRemoveAID.setBounds(272, 273, 421, 25);
		frame.getContentPane().add(textRemoveAID);
		
		JTextArea textRemoveAPID = new JTextArea();
		textRemoveAPID.setToolTipText("Enter holder ID");
		textRemoveAPID.setBounds(272, 311, 421, 25);
		frame.getContentPane().add(textRemoveAPID);
		
		JButton btnRemoveAccount = new JButton("Remove Account");
		btnRemoveAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				b.removeAccount(Integer.parseInt(textRemoveAPID.getText()),Integer.parseInt(textRemoveAID.getText()));
			}
		});
		btnRemoveAccount.setBounds(103, 272, 157, 25);
		frame.getContentPane().add(btnRemoveAccount);
		
		JTextArea textGetAData = new JTextArea();
		textGetAData.setToolTipText("Enter account ID");
		textGetAData.setBounds(272, 362, 421, 25);
		frame.getContentPane().add(textGetAData);
		
		JTextArea textArea_3 = new JTextArea();
		textArea_3.setToolTipText("Enter holder ID");
		textArea_3.setBounds(272, 400, 421, 25);
		frame.getContentPane().add(textArea_3);
		
		JButton btnGetdata = new JButton("Get Account Data");
		btnGetdata.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				b.getAccount(Integer.parseInt(textArea_3.getText()),Integer.parseInt(textGetAData.getText()));
				}
				catch(NumberFormatException e1){
					textFieldConsole.setText("Invalid format!");
					e1.printStackTrace();
				}
			}
		});
		btnGetdata.setBounds(103, 361, 157, 25);
		frame.getContentPane().add(btnGetdata);
		
		JLabel label = new JLabel("Accounts");
		label.setFont(new Font("Tahoma", Font.BOLD, 18));
		label.setBounds(12, 265, 100, 33);
		frame.getContentPane().add(label);
		
		JTextArea textArea_1 = new JTextArea();
		textArea_1.setToolTipText("Enter amount");
		textArea_1.setBounds(272, 525, 421, 25);
		frame.getContentPane().add(textArea_1);
		
		JTextArea textArea_4 = new JTextArea();
		textArea_4.setToolTipText("Enter account ID");
		textArea_4.setBounds(272, 449, 421, 25);
		frame.getContentPane().add(textArea_4);
		
		JTextArea textArea_5 = new JTextArea();
		textArea_5.setToolTipText("Enter holder ID");
		textArea_5.setBounds(272, 487, 421, 25);
		frame.getContentPane().add(textArea_5);
		
		textFieldConsole = new JTextField();
		textFieldConsole.setEnabled(false);
		textFieldConsole.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textFieldConsole.setBounds(727, 59, 471, 117);
		frame.getContentPane().add(textFieldConsole);
		textFieldConsole.setColumns(10);
		
		JLabel lblAccountsTable = new JLabel("Accounts Table");
		lblAccountsTable.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblAccountsTable.setBounds(727, 189, 161, 33);
		frame.getContentPane().add(lblAccountsTable);
		
		table = new JTable(accModel);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					textFieldConsole.setText("You clicked on the Accounts JTable.");
				}
			}
		});
		table.setEnabled(false);
		table.setBounds(727, 235, 471, 128);
		frame.getContentPane().add(table);
		
		JButton btnNewButton_1 = new JButton("Deposit");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				b.deposit(Integer.parseInt(textArea_4.getText()), Integer.parseInt(textArea_5.getText()), Integer.parseInt(textArea_1.getText()));
				}
				catch(NumberFormatException e2){
					textFieldConsole.setText("Invalid format!");
					e2.printStackTrace();
				}
			}
		});
		btnNewButton_1.setBounds(103, 448, 157, 25);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Withdraw");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				b.withdraw(Integer.parseInt(textArea_4.getText()), Integer.parseInt(textArea_5.getText()), Integer.parseInt(textArea_1.getText()));
				}
				catch(NumberFormatException e2){
					textFieldConsole.setText("Invalid format!");
					e2.printStackTrace();
					}
				}
		});
		btnNewButton_2.setBounds(103, 486, 157, 25);
		frame.getContentPane().add(btnNewButton_2);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(1177, 59, 21, 117);
		frame.getContentPane().add(scrollBar);
		
		JButton btnAddSpendingAccount = new JButton("Add spending account");
		btnAddSpendingAccount.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnAddSpendingAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
				b.addSpendingAccount(Integer.parseInt(textAddAPID.getText()),Integer.parseInt(textAddAID.getText()));
				}
				catch(NumberFormatException e1){
					textFieldConsole.setText("Invalid format!");
					e1.printStackTrace();
				}
			}
		});
		btnAddSpendingAccount.setBounds(103, 218, 157, 25);
		frame.getContentPane().add(btnAddSpendingAccount);
		
		JLabel lblPersosTable = new JLabel("Persons Table");
		lblPersosTable.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblPersosTable.setBounds(727, 376, 161, 33);
		frame.getContentPane().add(lblPersosTable);
		
		table_1 = new JTable(persModel);
		table_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					textFieldConsole.setText("You clicked on the Persons JTable.");
				}
			}
		});
		table_1.setEnabled(false);
		table_1.setBounds(727, 422, 471, 128);
		frame.getContentPane().add(table_1);
	}
	
	public void refreshPersons() {
		
		persModel.setRowCount(0);
		accModel.setRowCount(0);
		
		for (Integer a : b.persons.keySet()) {
			Person p;
			p = b.persons.get(a);
			String[] aux = {p.getName(), Integer.toString(p.getID())};
			persModel.addRow(aux);
			for (Account acc: p.accs) {
				String[] aux2 = {acc.getHolder().getName(), Integer.toString(acc.getBalance()), acc.toString(), Integer.toString(acc.getID())};
				accModel.addRow(aux2);
			}
		}
		
		//table.repaint();
		//table_1.repaint();
	}
	
	

}



